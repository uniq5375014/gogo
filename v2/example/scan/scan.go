package main

import (
	"gitlab.com/uniq5375014/gogo/v2/internal/plugin"
	"gitlab.com/uniq5375014/gogo/v2/pkg"
)

func main() {
	result := pkg.NewResult("127.0.0.1", "80")
	plugin.Dispatch(result)

	if result.Open {
		println(result.FullOutput())
	} else {
		println(result.GetTarget(), "close")
	}
}
