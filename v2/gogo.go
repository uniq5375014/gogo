//go:generate go run templates/templates_gen.go -t templates -o pkg/templates.go
package main

import (
	"gitlab.com/uniq5375014/gogo/v2/cmd"
)

func main() {
	//cpufile, _ := os.Create("cpu.prof")
	//pprof.StartCPUProfile(cpufile)
	//defer pprof.StopCPUProfile()
	//go func() {
	//	http.ListenAndServe("localhost:6060", nil)
	//}()
	cmd.Gogo()
}
